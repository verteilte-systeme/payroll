### curl commands

## Get Employees
`curl -v localhost:8080/employee`

## Create new Employee
`curl -X POST localhost:8080/employees -H 'Content-type:application/json' -d '{"name": "Samwise Gamgee", "role": "gardener"}'`

## Get Employee with id
`curl -v localhost:8080/employees/1`

## Update Employee
`curl -X PUT localhost:8080/employees/3 -H 'Content-type:application/json' -d '{"name": "Samwise Gamgee", "role": "ring bearer"}'`

## Delete Employee with id
`curl -X DELETE localhost:8080/employees/3`